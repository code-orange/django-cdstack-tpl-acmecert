from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)
from django_cdstack_tpl_deb_buster.django_cdstack_tpl_deb_buster.views import (
    handle as handle_deb_buster,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_acmecert/django_cdstack_tpl_acmecert"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    letsencrypt_apis = ("acme-v01.api.letsencrypt.org",)

    for api in letsencrypt_apis:
        path_api_dir = "etc/letsencrypt/accounts/" + api + "/directory/"
        path_full = path_api_dir + template_opts["letsencrypt_account_id"]

        files = ("meta.json", "regr.json", "private_key.json")

        for file in files:
            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/letsencrypt/accounts/"
                + file,
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                path_full + "/" + file,
                config_template.render(template_opts),
            )

        generate_config_static(zipfile_handler, template_opts, module_prefix)
        handle_whclient(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
        handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
        handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
        handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
